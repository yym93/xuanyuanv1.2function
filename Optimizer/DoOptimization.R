DoOptimization <- function(char_method, target_function, vec_lower, vec_upper,
                           vec_step_size, vec_initiate = NA, A = NA, b = NA, 
                           char_break_condition = "time", break_condition = 0.002,
                           char_disp = "Off", number_cluster = 1, repeat_time = 1){
#     Description:
#         This is a gathering of different optimizing methods.
#     
#     Inputs:
#         char_method: currently only "SGD"(for StochasticGradientDescend) and
#                      "Traversal" are available.
#         target_function: the function to be optimized, the input of the function
#                              should be a vector, while the output should be a scalar.
#         vec_lower: the lower bound of x, can't be -inf.
#         vec_upper: the upper bound of x, can't be inf.
#         vec_step_size: regularization vector of each parameter.
#         vec_initiate: an initial parameter vector(n*1) to start optimization, if empty,
#                       it starts with the avg of lower and upper bound.
#         A: inequality constraint as of the form A*x<=b, NA is allowed.
#         b: as above.
#         char_break_condition: options are 'TargetResult', 'Time', or
#                               'Count', it is a condition which, if satisfied,
#                               will break the iteration;note that break condition
#                               won't be effective for method 'ErgodicWalk'.
#         break_condition: if char_break_condition is 'TargetResult', break_condition
#                         shoulbe be a number as a goal for the optimization;
#                         if char_break_condition is 'Time', break_condition should be a
#                         maximum number of minutes for the optimization;
#                         if char_break_condition is 'Count', break_condition should be a
#                         maximum number of iterations for the optimization;
#                         note that break condition won't be effective for
#                             method 'ErgodicWalk'.
#         char_disp: if "Off", it prevents the function from displaying results of each iteration.
#         number_cluster: number of clusters to use when optimizing, the max number
#                         of it depends on your computer.
#         repeat_time: automatically optimize repeat_time times, outputting the optimized
#                      value and parameters for each time.
#     
#     Outputs:
#         optimized.result: the minimun found till breaking, if repeat_time > 1,
#                           this is a vector.
#         vec_parameters: the parameters yielding optimized.result, if repeat_time > 1,
#                         this is a matrix whose columns are sets of parameters.
#     
#     Test Cases:
#         char_method <- "SGD"
#         A <- matrix(c(-1,0,1,0,0,1,0,-1),2,4)
#         b <- matrix(c(-3,-1),,1)
#         vec_lower <- rep(-100,4)
#         vec_upper <- rep(100,4)
#         vec_step_size <- c(1e-5, 1e-5, 1, 1)
#         char_break_condition <- "time"
#         break_condition <- 0.001
#         char_disp <- "on"
#         DoOptimization(char_method, DoExample, vec_lower, vec_upper,
#         vec_step_size, NA, A, b, char_break_condition, break_condition, char_disp,
#         number_cluster = 5, repeat_time = 4)
#     
#     Test Results:
#         [1] "Starting optimizing for the 1st time..."
#         [1] "Result: 0.00160680823243675"
#         [1] "Starting optimizing for the 2nd time..."
#         [1] "Result: 0.000359952209598391"
#         [1] "Starting optimizing for the 3th time..."
#         [1] "Result: 0.000235605108293235"
#         [1] "Starting optimizing for the 4th time..."
#         [1] "Result: 0.000579189506321631"
#         [[1]]
#         [1] 0.0016068082 0.0003599522 0.0002356051 0.0005791895
#         
#         [[2]]
#         [,1]     [,2]     [,3]    [,4]
#         [1,]  5.50319  5.29684  4.62004 5.29966
#         [2,]  2.31477 -0.00467 -0.01289 1.86149
#         [3,]  5.00000 -2.00000 -3.00000 2.00000
#         [4,] 10.00000  5.00000  4.00000 4.00000
#         
#     History:
#         Yiming Yu 2014/09/14: Adapted this function from MATLAB  frame.
#         Yiming Yu 2014/10/09: Added the parallel cluster number option.
#         YiminG Yu 2014/10/09: Changed 'ErgodicWalk' to 'Traversal'.
#         Yiming Yu 2014/10/10: Added the repeat_time option.
    
    n.method <- 0
    vec_method <- c("SGD", "Traversal")
    for(i in 1:length(vec_method)){
        temp <- grep(char_method, vec_method[i], ignore.case = TRUE)
        temp <- temp[1]
        if(!is.na(temp)){
            n.method <- i
        }
    }
    if(repeat_time == 1){
        if(n.method == 1){
            result <- DoStochasticGradientDescend(target_function, vec_initiate, A,
                                                  b, vec_lower, vec_upper, vec_step_size,
                                                  char_break_condition, break_condition,
                                                  char_disp, number_cluster)
        }else if(n.method == 2){
            result <- DoErgodicWalk(target_function, A, b, vec_lower, vec_upper,
                          vec_step_size, char_disp, number_cluster)
        }
        return(list(result[[1]], matrix(result[[2]], , 1)))
    }else{
        char_suffix <- c("st", "nd", "th")
        b_disp <- !is.na(grep(char_disp, 'on', ignore.case = TRUE)[1])
        vec_optimized_result <- rep(NA, repeat_time)
        mat_parameters <- matrix(NA, length(vec_lower), repeat_time)
        for(i in 1:repeat_time){
            if(b_disp){
                print(paste("Starting optimizing for the ", i, char_suffix[min(i, 3)], " time...", sep = ''))
            }
            if(n.method == 1){
                result <- DoStochasticGradientDescend(target_function, vec_initiate, A,
                                                      b, vec_lower, vec_upper, vec_step_size,
                                                      char_break_condition, break_condition,
                                                      'off', number_cluster)
            }else if(n.method == 2){
                if(b_disp){
                    print("WARNING: NO ONE WANTS TO REPEATEDLY WHILE TRAVERSALLY OPTIMIZE")
                }
                result <- DoErgodicWalk(target_function, A, b, vec_lower, vec_upper,
                                        vec_step_size, 'off', number_cluster)
            }
            if(b_disp){
                print(paste("Result: ", result[[1]], sep = ''))
            }
            vec_optimized_result[i] <- result[[1]]
            mat_parameters[, i] <- result[[2]]
        }
        return(list(vec_optimized_result, mat_parameters))
    }
    return()
}