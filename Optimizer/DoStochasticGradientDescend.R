DoStochasticGradientDescend <- function(target_function, vec_initiate, A,
                                         b, vec_lower, vec_upper, vec_step_size,
                                         char_break_condition, break_condition,
                                         char_disp, number_cluster){
    
#     History:
#         Yiming Yu 2014/09/14: Adapted this function from MATLAB  frame.
#         Yiming Yu 2014/10/09: Added parallel computing option.
    
    cl <- makeCluster(getOption("cl.cores", number_cluster))
    clusterExport(cl = cl, ls(.GlobalEnv))
    n_break_condition <- 0
    vec_not_do <- (vec_step_size == 0)
    char_break_all <- c("targetresult", "time", "count")
    for(i in 1:length(char_break_all)){
        temp <- grep(char_break_condition, char_break_all[i], ignore.case <- TRUE)
        temp <- temp[1]
        if(!is.na(temp)){
            n_break_condition <- i
        }
    }
    ##
    start_time <- proc.time()
    start_time <- start_time[3]
    b.disp <- 1
    temp <- grep(char_disp, "off", ignore.case <- TRUE)[1]
    if(!is.na(temp)){
        b.disp <- 0
    }
    vec_original_initiate <- vec_initiate
    N <- length(vec_lower)
    vec_lower <- matrix(vec_lower,,1)
    vec_upper <- matrix(vec_upper,,1)
    vec_step_size <- matrix(vec_step_size,,1)
    vec_lower <- vec_step_size * ceiling(vec_lower / vec_step_size)
    vec_upper <- vec_step_size * floor(vec_upper / vec_step_size)
    if(!is.na(vec_initiate[1])){
        vec_initiate <- matrix(vec_initiate,,1)
        vec_initiate <- vec_step_size * round(vec_initiate / vec_step_size)
    }else{
        vec_initiate <- vec_step_size * round(rowMeans(matrix(c(vec_lower, vec_upper),,2)) /
                                                  vec_step_size)
    }
    vec_initiate[vec_not_do] <- vec_original_initiate[vec_not_do]
    vec_initiate <- DoConditionTest(vec_initiate, A, b)
    temp1 <- 1
    while(!ncol(vec_initiate) &(temp1 < 1e4)){
        temp <- vec_step_size * round((vec_lower + runif(N) * (vec_upper - vec_lower)) /
                                          vec_step_size)
        vec_initiate <- DoConditionTest(temp, A, b)
        temp1 <- temp1 + 1
    }
    if(!ncol(vec_initiate)){
        return("Unable to generate an initial vector in 1e4 times, please fill vec_initiate or try more times")
    }
    ##
    multiplier <- 0.75
    ##
    count <- 0
    vec_initiate[vec_not_do] <- vec_original_initiate[vec_not_do]
    vec_var <- vec_initiate
    vec_delta <- pmax(vec_upper - vec_var, vec_var - vec_lower)
    mat_history <- vec_var
    optimized_result <- target_function(vec_var)
    n_exit <- 0
    ##
    while(!n_exit){
        index_var <- count%%N + 1
        while(vec_not_do[index_var]){
            index_var <- (index_var + 1) %% N
        }
        mat_var <- cbind(vec_var, vec_var)
        mat_var[index_var, 1] <- max(vec_lower[index_var], vec_step_size[index_var] *
                                         round((mat_var[index_var, 1] - vec_delta[index_var]) /
                                         vec_step_size[index_var]))
        mat_var[index_var, 2] <- min(vec_upper[index_var], vec_step_size[index_var] *
                                         round((mat_var[index_var, 1] + vec_delta[index_var]) /
                                         vec_step_size[index_var]))
        mat_var <- t(unique(t(mat_var)))
        mat_var <- DoConditionTest(mat_var, A, b)
        mat_var <- DoRepeatTest(mat_var, mat_history)
        delta_size <- ncol(mat_var)
        ##
        temp1 <- delta_size
        temp3 <- 1
        while((temp1 < max(delta_size + 1, number_cluster)) & (temp3 < 1e4)){
            temp2 <- vec_var
            temp2[index_var] <- vec_step_size[index_var] * round((vec_lower[index_var] +
                                runif(1) * (vec_upper[index_var] - vec_lower[index_var])) /
                                vec_step_size[index_var])
            if((temp2[index_var] >= vec_lower[index_var]) & (temp2[index_var] <= vec_upper[index_var])){
                mat_var <- cbind(mat_var, temp2)
            }
            mat_var <- t(unique(t(mat_var)))
            mat_var <- DoConditionTest(mat_var, A, b)
            mat_var <- DoRepeatTest(mat_var, mat_history)
            temp1 <- ncol(mat_var)
            temp3 <- temp3 + 1
        }
        ##
        if(ncol(mat_var) > 0){
            if((ncol(mat_var) > 1) | ((ncol(mat_var) == 1) & (sum(mat_var[,1] != vec_var) > 0))){
                temp <- parCapply(cl = cl, mat_var, target_function)
                #
#                 list_var <- mat_var[, 1]
#                 if(ncol(mat_var) > 1){
#                     for(i in 2:ncol(mat_var)){
#                         list_var <- c(list_var, mat_var[, i])
#                     }
#                 }
#                 temp <- parCapply(cl, list_var, target_function)
                #
                mat_history <- cbind(mat_history, mat_var)
                C <- min(temp)
                I <- which.min(temp)
                if(C < optimized_result){
                    optimized_result <- C
                    vec_var <- mat_var[,I]
                    if(I > delta_size){
                        vec_delta[index_var] <- max(abs(vec_var[index_var] - mat_var[index_var, I]),
                                                        vec_delta[index_var] * multiplier)
                    }else{
                        vec_delta[index_var] <- vec_delta[index_var] * multiplier
                    }
                }else{
                    vec_delta[index_var] <- vec_delta[index_var] * multiplier
                }
            }
        }else{
            vec_delta[index_var] <- vec_delta[index_var] * multiplier
        }
        count <- count + 1
        if(b.disp){
            print(paste("current min: ",optimized_result, ", count: ", count,
                        ", parameters: ", sep = ''))
            print(matrix(vec_var,1,))
        }
        ##
        if(n_break_condition == 1){
            if(optimized_result <- break_condition){
                n_exit <- 1
            }
        }else if(n_break_condition == 2){
            now_time <- proc.time()
            now_time <- now_time[3]
            if(now_time - start_time > break_condition * 60){
                n_exit <- 1
            }
        }else if(n_break_condition == 3){
            if(count >= break_condition){
                n_exit <- 1
            }
        }
    }
    if(b.disp){
        now_time <- proc.time()
        print(paste("elspased time for DoStochasticGradientDesend is ", now_time[3] -
                        start_time,"seconds", sep <- ""))
    }
    vec_parameters <- vec_var
    stopCluster(cl)
    return(list(optimized_result, vec_parameters))
}