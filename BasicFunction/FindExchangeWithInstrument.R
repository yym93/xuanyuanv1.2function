findExchangeWithIntrument = function(instrument_name = "IF"){
  #       Description:
  #           This function returns the name of the exchange
  # 
  #       Test Cases:
  #           findExchangeWithIntrument("CF")
  #           
  #       Test Results:
  #           "CZCE_DAILY"
  #       History:
  #           Zhen Zhou 2014/09/16: . 
  if(instrument_name %in% list("IF", "TF")){ 
    return("CFFEX_DAILY") 
  }else if(instrument_name %in% list("CF","TA","FG","RM","WS","WH","ME","MA","RO","OI")){ 
    return("CZCE_DAILY") 
  }else if(instrument_name %in% list("y","a","m","jm","c","j","l","p","jm","jd","i","bb","fb","pp")){
    return("DCE_DAILY")
  }else if(instrument_name %in% list("cu","al","zn","rb","ru")){
    return("SHFE_DAILY")
  }else{
    cat("no such instrument_name", instrument_name)
    return(NULL)
  }
}