Index2Time_IF <- function(index){
#     Description:
#         Transfers index to time for IF.
#     
#     Inputs:
#         index: an integer from 1 to 32403.
#     
#     Outputs:
#         time: an integer as in hhmmssmmm(hour\minute\second\millisecond) format.
#     
#     Test Cases:
#         Index2Time_IF(300)
#     
#     Test Results:
#         [1] 91729000
#     
#     History:
#         Yiming Yu 2014/09/23: Separated this function from Index2Time.
    
    if(index==1){
        time <- 91400000
    }else if(index<=16202){
        millisec <- 500*(index %% 2)
        index <- index-2
        sec <- floor(index/2) %% 60
        min <- 15+floor(index/120)
        hour <- 9+floor(min/60)
        min <- min %% 60
        time <- 1e7*hour+1e5*min+1e3*sec+millisec
    }else{
        millisec <- 500*(1-(index %% 2))
        sec <- floor((index-16203)/2) %% 60
        min <- floor((index-16203)/120)
        hour <- 13+floor(min/60)
        min <- min %% 60
        time <- 1e7*hour+1e5*min+1e3*sec+millisec
    }
    return(time)
}