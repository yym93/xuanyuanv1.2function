Time2Index_Commodity <- function(time){
#     Description:
#         Transfers index to time for Commodity.
#     
#     Inputs:
#         time: an integer as in hhmmssmmm(hour\minute\second\millisecond) format.
#     
#     Outputs:
#         index: an integer from 1 to 27004.
#     
#     Test Cases:
#         Time2Index_Commodity(101500500)
#     
#     Test Results:
#         [1] 9002
#     
#     History:
#         Yiming Yu 2014/09/24: Created this function.
    
    hour <- floor(time / 1e7)
    min <- floor(time / 1e5) %% 1e2
    sec <- floor(time / 1e3) %% 1e2
    millisec <- time %% 1e3
    if(hour == 8 & min == 59 & sec == 0 && millisec == 0){
        return(1)
    }else{
        temp <- hour * 1e2 + min
        if(temp <= 1015){
            index <- 1 + (hour - 9) * 7200 + min * 120 + sec * 2 + (millisec >= 500)
            if(index < 2 | index > 9002){
                return(-1)
            }
        }else if(temp <= 1130){
            index <- 9002 + (hour - 10) * 7200 + (min - 30) * 120 + sec * 2 + (millisec >= 500)
            if(index < 9003 | index > 16203){
                return(-1)
            }
        }else{
            index <- 16203 + (hour - 13) * 7200 + (min - 30) * 120 + sec * 2 + (millisec >= 500)
            if(index < 16204 | index > 27004){
                return(-1)
            }
        }
        return(index)
    }
}