ExtractDigitFromString <- function(char_raw){
    
    vec_raw <- NULL
    for(i in 1:length(char_raw)){
        number <- NULL
        for(j in 1:nchar(char_raw[i])){
            temp <- substr(char_raw[i], j, j)
            if(!is.na(as.integer(temp))){
                number <- paste(number, temp, sep = '')
            }
        }
        vec_raw <- c(vec_raw, as.integer(number))
    }
    return(vec_raw)
}