CalcTradeCashFlow <- function(nmatMatchedOrder,dmatPrice,dvecCost){
#     Description:
#         calculates cash flow on open spot and close spot.
#     
#     Inputs:
#         mat.matched.order: matrix of matched order
#         mat.price: price.matrix
#         vec.cost: cost vector
#     
#     Output:
#         mat.trade.cash.flow: trade cash flow matrix
#     
#     Test Cases:
#         nmatMatchedOrder=matrix(c(1,3,3,4,2,5,6,6,1,1,1,1),4,3)
#         dmatPrice=matrix(c(1,2,3,2,3,4,3,4,5),,1)
#         dvecCost=c(2.5e-5,0,0)
#         CalcTradeCashFlow(nmatMatchedOrder,dmatPrice,dvecCost)
#         
#     Test Results:
#         [,1]     [,2]
#         [1,] -1.000075 1.999850
#         [2,] -3.000225 2.999775
#         [3,] -3.000225 3.999700
#         [4,] -2.000150 3.999700
#         
#     History:
#         Yiming Yu 2014/18/29: Adapted this function from MATLAB frame

    dSlip=2
    nmatTradeOpenCF <- -dmatPrice[nmatMatchedOrder[,1]]*nmatMatchedOrder[,3]-(1+dSlip)*(dvecCost[1]*dmatPrice[nmatMatchedOrder[,1]]+dvecCost[2])*abs(nmatMatchedOrder[,3])
    bmatInterDay <- floor((nmatMatchedOrder[,1]-1)/32402)==floor((nmatMatchedOrder[,2]-1)/32402)
    nmatTradeCloseCF <- dmatPrice[nmatMatchedOrder[,2]]*nmatMatchedOrder[,3]-(1+dSlip)*(1-dvecCost[3]*bmatInterDay)*(dvecCost[1]*dmatPrice[nmatMatchedOrder[,2]]+dvecCost[2])*abs(nmatMatchedOrder[,3])
    nmatTradeCashFlow <- matrix(c(nmatTradeOpenCF,nmatTradeCloseCF),length(nmatTradeCloseCF),2)
    return(nmatTradeCashFlow)
}