Index2Time_Commodity <- function(index){
#     Description:
#         Transfers index to time for Commodity.
#     
#     Inputs:
#         index: an integer from 1 to 27004.
#     
#     Outputs:
#         time: an integer as in hhmmssmmm(hour\minute\second\millisecond) format.
#     
#     Test Cases:
#         Index2Time_Commodity(9002)
#     
#     Test Results:
#         [1] 101500500
#     
#     History:
#         Yiming Yu 2014/09/24: Created this function.
    
    if(index == 1){
        return(85900000)
    }else if(index <=9002){
        millisec <- 500 * ((index + 1) %% 2)
        index <- index - 1
        sec <- floor(index / 2) %% 60
        min <- floor(index / 120)
        hour <- 9 + floor(min / 60)
        min = min %% 60
        return(hour * 1e7 + min * 1e5 + sec * 1e3 + millisec)
    }else if(index <= 16203){
        millisec <- 500 * (index %% 2)
        index <- index - 9002
        sec <- floor(index / 2) %% 60
        min <- 30 + floor(index / 120)
        hour <- 10 + floor(min / 60)
        min <- min %% 60
        return(hour * 1e7 + min * 1e5 + sec * 1e3 + millisec)
    }else{
        millisec <- 500 * ((index + 1) %% 2)
        index <- index - 16203
        sec <- floor(index / 2) %% 60
        min <- 30 + floor(index / 120)
        hour <- 13 + floor(min / 60)
        min <- min %% 60
        return(hour * 1e7 + min * 1e5 + sec * 1e3 + millisec)
    }
}