GetStarted <- function(){
#     Description:
#         Load required library and source all functions in the backtest frame
#     
#     Inputs:
#         None.
#         
#     Outputs:
#         None.s   
#         
#     Test Cases:
#         Just run it.
#     
#     History:
#         Yiming Yu 2014/09/15: Created this function.
#         Zhen Zhou 2014/10/18 : modified to be more interpretable.
#         Yiming Yu 2014/10/22: Robustened the packages loading.
    
    ## Install and library packages
    char_packages_to_library <- c("rJava", "numbers", "snow", "RMongo", "lubridate",
                                  "gridExtra", "ggplot2")
    char_installed_packages <- installed.packages()[, "Package"]
    for(i in 1:length(char_packages_to_library)){
        if(!char_packages_to_library[i] %in% char_installed_packages){
            install.packages(char_packages_to_library[i])
        }
        library(char_packages_to_library[i], character.only = TRUE, quietly = TRUE)
    }
    ## Source files
    file.names <- list.files(recursive = TRUE)
    for(i in 1:length(file.names)){
        isR <- !is.na(grep(".R", file.names[i])[1])
        isRData <- !is.na(grep(".RData", file.names[i])[1])
        isInStrategy <- !is.na(grep("Strategy", file.names[i])[1])
        isRun <- !is.na(grep("Run", file.names[i])[1])
        isTrain <- !is.na(grep("Train", file.names[i])[1])
        isHelper <- !is.na(grep("Helper", file.names[i])[1])
        
        #         if the file is .R, read it,
        #         if it is .RData, do not read it
        #         if it includes Run, and is in the Strategy file, do not read it
        #         if it includes Train and is not a TrainHelper, do not read it
        
        if(isR & !isRData & !(isInStrategy & isRun) & !(isTrain & !isHelper) & !isRun){
            source(file.names[i])
        }
    }
}