vec_parameters <- c(20, 1, 120)
start_date <- 20140631
end_date <- 20140731
char_commodity_type <- "IF"
char_contract_type <- c("CurrentMonthContinuous", "NextMonthContinuous")
char_strategy_name <- "Ito Bollinger Spread"
bar_size <- vec_parameters[3]
##
mat_price_1 <- GetDataFromLocal(start_date, end_date, char_commodity_type, 1,
                                "Price", char_contract_type[1],
                                days_ahead = 3)
mat_price_2 <- GetDataFromLocal(start_date, end_date, char_commodity_type, 1,
                                "Price", char_contract_type[2],
                                days_ahead = 3)
list_price <- list(mat_price_1, mat_price_2)
##
list_order <- CalcItoBollingerSpread(list_price, vec_parameters)
list_order <- ProcessOrder(start_date, end_date, list_order, char_commodity_type,
                           char_contract_type, bar_size)
##
list_result <- AnalyzeStrategy_Accumulated(start_date, end_date, list_price, list_order,
                                           1e7, fixed_interest_rate = 0,
                                           leverage = c(1,1), margin_rate = c(0.13,0.13), window_size=0,
                                           char_commodity_type=c("IF", "IF"), char_strategy_name = char_strategy_name)
list_result <- AnalyzeStrategy_Unit(start_date, end_date, list_price, list_order, window_size=0,
                                    char_pnl_type="Daily", char_commodity_type=c("IF","IF"),
                                    char_contract_type,
                                    days_ahead=c(3,3), char_strategy_name = char_strategy_name)