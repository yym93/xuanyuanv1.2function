vec_parameters <- c(16, 0.5, 0.3, 0.01, 0.01, 120)# window_size, upperK, lowerK, stop.profit, stop.loss
start_date <- 20110101
end_date <- 20140330
char_commodity_type <- "RB"
char_contract_type <- "Major1"
char_strategy_name <- "Dual Thrust"
bar_size <- vec_parameters[6]
##
mat_price <- GetDataFromLocal(start_date, end_date, char_commodity_type, 1,
                                "Price", char_contract_type,
                                days_ahead = 3)
##
mat_order <- CalcDualThrust(mat_price, vec_parameters)
mat_order <- ProcessOrder(start_date, end_date, mat_order, char_commodity_type,
                          char_contract_type, bar_size)
##
list.result <- AnalyzeStrategy_Accumulated(start_date, end_date, mat_price, mat_order,
                                           1e7, fixed_interest_rate = 0,
                                           leverage = c(5), margin_rate = c(0.13), window_size = vec_parameters[1],
                                           char_commodity_type=char_commodity_type, char_strategy_name = char_strategy_name)
list.result <- AnalyzeStrategy_Unit(start_date, end_date, mat_price, mat_order, window_size = vec_parameters[1],
                                    char_pnl_type="Daily", char_commodity_type=char_commodity_type,
                                    char_contract_type = char_contract_type,
                                    days_ahead=c(3), char_strategy_name = char_strategy_name)