TrainBayesianSpreadHelper <- function(vec_parameters){
    
    library('numbers')
    char.main.path <- GetMainPath()
    load(paste(char.main.path, '/Data/TempTraining.RData', sep = ''))
    mat_order <- CalcBayesianSpread(mat_price, vec_parameters)
    mat_order <- ProcessOrder(mat_order, char_commodity_type)
    ##
    vec_cost <- GetCost(char_commodity_type)
    vec_daily_pnl <- CalcDailyPnL(mat_price, mat_order, vec_cost)
    return(-CalcSharpeRatio(vec_daily_pnl))
}