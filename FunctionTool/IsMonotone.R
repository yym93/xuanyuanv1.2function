IsMonotone <- function(vec_data){
#     Description:
#         Determine whether a vector is monotone.
#     
#     Inputs:
#         vec.data: raw data series.
#     
#     Outputs:
#         True/FALSE.
#     
#     Test Cases:
#         IsMonotone(1:10)
#         IsMonotone(10:1)
#         IsMonotone(c(1,2,3,4,5,3))
#     
#     Test Results:
#         > IsMonotone(1:10)
#         [1] TRUE
#         > IsMonotone(10:1)
#         [1] TRUE
#         > IsMonotone(c(1,2,3,4,5,3))
#         [1] FALSE
#         
#     History:
#         Yiming Yu 2014/10/04: Adapted this function from MATLAB frame.
    
    N <- length(vec_data)
    return((sum((vec_data[1:(N - 2)] - vec_data[2:(N - 1)]) * (vec_data[2:(N - 1)] - vec_data[3:N]) < 0) == 0))
}